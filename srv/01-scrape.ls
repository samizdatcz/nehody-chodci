require! {
  fs
  async
}

dir = fs.readdirSync "#__dirname/../data/scraped"
# dir .= slice 1, 2
pattern = 'block;width:120px;">
  ([0-9]{10,})
  </a\><span style="display:inline-block;width:170px;"\>
  ([\.0-9: ]+) [^<]+
  </span><a onclick="javascript:zoomMapByPoint\\(\'POINT\\(([-0-9\.]+) ([-0-9\.]+)\\)
  '
rxG = new RegExp pattern, 'gi'
rxL = new RegExp pattern, 'i'
out = "id\tdate\tx\ty\n"
<~ async.eachSeries dir, (file, cb) ->
  (err, body) <~ fs.readFile "#__dirname/../data/scraped/#file"
  body .= toString!
  matches = body.match rxG
  for m in matches
    parts = m.match rxL
    parts .= slice 1
    out += parts.join "\t"
    out += "\n"

  cb!
fs.writeFileSync "#__dirname/../data/scraped.tsv", out

