require! {
  fs
  async
}
files = fs.readdirSync "#__dirname/../data/pripady"
# files.length = 1
pattern = "<td><span>(.*?)</span></td>"
rxG = new RegExp pattern, 'gi'
rxPoint = new RegExp "MapCenterWkt=POINT\\(([-0-9\.]+) ([-0-9\.]+)\\)", 'i'
fields =
  "id"
  "obec"
  "datum"
  "komunikace_druh"
  "komunikace_cislo"
  "zavineni"
  "ridic_kategorie"
  "ridic_ovlivneni"
  "vinik_alkohol"
  "ridic_stav"
  "obeti_usmrceno"
  "obeti_lehce"
  "obeti_tezce"
  "vozidel_celkem"
  "vinik_vozidlo_znacka"
  "vinik_vozidlo_po_nehode"
  "skoda"
  "unik_hmot"
  "vinik_vozidlo_druh"
  "vinik_vozidlo_rok_vyroby"
  "vinik_vozidlo_vlastnik"
  "vinik_skoda_na_vozidle"
  "vyprosteni"
  "nehoda_charakter"
  "srazka_vozidel_druh"
  "hlavni_priciny"
  "povrch_stav"
  "meteo"
  "rozhled"
  "nehoda_poloha"
  "prednost_v_jizde"
  "smerove_podminky"
  "druh_krizujici_komunikace"
  "smer_jizdy"
  "nehoda_druh"
  "pevna_prekazka_druh"
  "povrch_druh"
  "komunikace_stav"
  "viditelnost"
  "komunikace_deleni"
  "rizeni_provozu"
  "objekty"
  "misto_nehody"
  "smyk"
  "x"
  "y"
out = fields.join "\t"
<~ async.eachSeries files, (file, cb) ->
  (err, data) <~ fs.readFile "#__dirname/../data/pripady/#file"
  data = data.toString!
  matches = data.match rxG
  values = for m in matches
    m.split "</" .0.split "span>" .1
  values.unshift (file.split "." .0)
  point = data.match rxPoint
  values.push point.1, point.2
  out += "\n"
  out += values.join "\t"
  cb!

fs.writeFileSync "#__dirname/../data/details.tsv", out
