casper = require('casper').create()
fs = require('fs')
ids = fs.read "../data/scraped.tsv"
ids = ids.split "\n"
ids = ids.map (it) -> it.split("\t")[0]
ids.pop()
ids.shift()
ids = ids.slice 4
randomizer = 1000
getContent = ->
  document.body.innerHTML
casper
  .start("http://maps.jdvm.cz/cdv2/apps/nehodyvmape/Search.aspx")
  .eachThen ids, (response) ->
    id = response.data
    randomizer++
    console.log "Opening", id
    @thenOpen "http://maps.jdvm.cz/cdv2/apps/nehodyvmape/Detail.aspx?objid=#{id}&WinName=#{randomizer}", (response) ->
      content = @getPageContent()
      # content = @thenEvaluate getContent
      fs.write "../data/pripady/#{id}.html", content, 'w'

casper.run()
# console.log ids
# console.log ids
# ids.pop()
# ids.shift()
# console.log ids
