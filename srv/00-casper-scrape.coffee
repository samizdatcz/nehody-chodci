links = []
tabs = []
getLinks = ->
  document.body.innerHTML
  # links = document.querySelectorAll '#TcoMain_TpaResObject_TblResObject tr'
  # Array::map.call links, (e) -> e.innerHTML

getTabs = ->
  tabs = document.querySelectorAll 'select[name="TcoMain$TpaResObject$LbxResIntervalsObject"] option'
  Array::map.call tabs, (e) -> e.value

setTab = (tab) ->
  selector = document.querySelector('select[name="TcoMain$TpaResObject$LbxResIntervalsObject"]')
  selector.value = tab
  selector.onchange()

casper = require('casper').create()
fs = require('fs')
casper.start "http://maps.jdvm.cz/cdv2/apps/nehodyvmape/Search.aspx"
casper.thenEvaluate ->
  document.querySelector('select[name="TcoMain$TpaObject$DdlDruhNehody"]').selectedIndex = 5
  document.querySelector('input[name="TcoMain$TpaObject$TxtOkres"]').setAttribute('value', "CZ0100 Praha (Hlavní město Praha)")
  document.querySelector('input[name="TcoMain$TpaObject$BtnSearch"]').click()
  @echo "prdel"

casper.waitForSelector "td.tdResFill", ->
  tabs = @evaluate getTabs
  tabs = tabs.reverse()
  console.log(tabs)
  links = @evaluate getLinks
  # fs.write "./data/test.html", links, 'w'
  casper.eachThen tabs, (response) ->
    tab = response.data
    console.log(tab)
    @thenEvaluate setTab, tab
    @waitForSelectorTextChange "td.tdResFill", ->
      console.log("CHANGE")
      links = @evaluate getLinks
      fs.write "./data/test-#{tab}.html", links, 'w'
      console.log(links.length)
      console.log "waiting"
    # @wait 2000, ->
    #   console.log "done waiting"

casper.run ->
  @exit()
